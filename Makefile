.PHONY: orig deb

VERSION := 0.9.1

update:
	git submodule update --remote

orig:
	tar --exclude=blitz/.git -czf ../php5.6-blitz_$(VERSION).orig.tar.gz blitz

deb:
	dpkg-buildpackage -b -tc

# [EOF]
